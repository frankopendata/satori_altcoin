'use strict';

var RTM = require('satori-sdk-js');
var http = require('http'),
    request = require('request'),
    zlib = require('zlib'),
    inflate = require('./config/inflate.js'),
    config = require('./config/config.js'),
    fs = require('fs'),
    reqs = [];
var live = "";

var endpoint = "wss://open-data.api.satori.com";
var appkey = "appkeyHere";
var role = "altcoin-stats";
var roleSecretKey = "roleSecretKeyHere";
var channel = "altcoin-stats";

var roleSecretProvider = RTM.roleSecretAuthProvider(role, roleSecretKey);

var rtm = new RTM(endpoint, appkey, {
  authProvider: roleSecretProvider,
});

reqs[0] = config.coinmarketcap;

var headers = {
  "accept-charset" : "ISO-8859-1,utf-8;q=0.7,*;q=0.3",
  "accept-language" : "en-US,en;q=0.8",
  "accept" : "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
  "user-agent" : "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_6_8) AppleWebKit/537.13+ (KHTML, like Gecko) Version/5.1.7 Safari/534.57.2",
  "accept-encoding" : "gzip,deflate",
}
var options;

var subscription = rtm.subscribe(channel, RTM.SubscriptionMode.SIMPLE);

var compressedRequest = function(options, outStream) {
  var req = request(options, function(error, response, body){
    try {
    } catch (e) {
      console.log('parsing error' + e);
    }
  })

  req.on('response', function (res) {
    live = "";
    if (res.statusCode !== 200) throw new Error('Status not 200')
    var encoding = res.headers['content-encoding']
    if (encoding == 'gzip') {
      res.pipe(zlib.createGunzip())
      .setEncoding('utf8')
      .on('data', function(message){
          var test = message.replace(/[\n\r]+/g, '');
          var last1 = test.slice(-1);
          live = live + test;
          if (last1 == "]") {
            try {
              console.log('gzipresponse', new Date().toLocaleString());
              var jsondata = JSON.parse(live);
              responseParse(jsondata);
            } catch (e) {
              console.log('parsing error stocazzo' + e);
            }
    }
      })
    } else if (encoding == 'deflate') {
      res.pipe(inflate.createInflate())
         .setEncoding('utf8')
         .on('data', function(message){
          var test = message.replace(/[\n\r]+/g, '');
            try {
              console.log('defateresponse');
              var jsondata = JSON.parse(test);
              responseParse(jsondata);
            } catch (e) {
              console.log('parsing error' + e);
            }
        })
    } else {

    }
  })

  req.on('error', function(err) {
    throw err;
  })
}

var responseParse = function (jsondata){

  if (jsondata) {
    for (var i = 0; i < jsondata.length; i++) {
      publish(jsondata[i]);
    }
  }

}

var startStats = function (){
  console.log("startStats");
  for (var i = reqs.length - 1; i >= 0; i--) {
   options = {
     url : reqs[i],
     headers: headers
   };
    compressedRequest(options);
  } 
}

/* publish a message after being subscribed to sync on subscription */
subscription.on('enter-subscribed', function () {
  rtm.publish(channel, "Hello, World!", function (pdu) {

  });
  startStats();
});

/* set callback for PDU with specific action */
subscription.on('rtm/subscription/data', function (pdu) {
  pdu.body.messages.forEach(function (msg) {

  });
});

var publish = function(message){
  rtm.publish(channel, message, function (pdu) {

  });
}

rtm.start();

setInterval(function(){
  startStats();
}, 1000 * 60 * 5);