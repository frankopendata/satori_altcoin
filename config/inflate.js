'use strict'

var zlib = require('zlib')
  , stream = require('stream')
  , inherit = require('util').inherits
  , Inflate

Inflate = function (options) {
  this.options = options
  this._stream = null
  stream.Transform.call(this)
}

inherit(Inflate, stream.Transform)

Inflate.prototype._transform = function (chunk, encoding, callback) {
  var self = this
  if (!self._stream) {
    // If the response stream does not have a valid deflate header, use `InflateRaw`
    if ((new Buffer(chunk, encoding)[0] & 0x0F) === 0x08) {
      self._stream = zlib.createInflate(self.options)
    } else {
      self._stream = zlib.createInflateRaw(self.options)
    }

    self._stream.on('error', function (error) {
      self.emit('error', error)
    })

    self.once('finish', function () {
      self._stream.end()
    })
    self._stream.on('data', function (chunk) {
      self.push(chunk)
    })

    self._stream.once('end', function () {
      self._ended = true
      self.push(null)
    })
  }

  self._stream.write(chunk, encoding, callback)
}

Inflate.prototype._flush = function (callback) {
  if (this._stream && !this._ended) {
    this._stream.once('end', callback)
  } else {
    callback()
  }
}

module.exports.createInflate = function (options) {
  return new Inflate(options)
}
